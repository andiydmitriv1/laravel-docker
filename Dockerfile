FROM php:7.4.0-fpm-alpine
WORKDIR /var/www/html

ADD https://github.com/mlocati/docker-php-extension-installer/releases/latest/download/install-php-extensions /usr/local/bin/

COPY . /var/www/html

ARG user
ARG uid

RUN apk update && apk add \
    build-base \
    freetype-dev \
    libjpeg-turbo-dev \
    libpng-dev \
    libzip-dev \
    zip \
    jpegoptim optipng pngquant gifsicle \
    vim \
    unzip \
    git \
    oniguruma-dev \
    curl

RUN docker-php-ext-install pdo_mysql mbstring zip exif pcntl
RUN docker-php-ext-configure gd
RUN docker-php-ext-install gd


RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer


COPY --chown=www-data:www-data ./src /var/www/html
RUN touch /.dockerenv



ADD php/express.ini /usr/local/etc/php/conf.d


COPY --from=composer:latest /usr/bin/composer /usr/bin/composer

RUN composer install  -d /var/www/html/
RUN composer install --ignore-platform-reqs -d /var/www/html/ && \
    chown -R www-data:www-data /var/www/html/vendor && \
    chown -R www-data:www-data /var/www/html/composer.lock
RUN composer update

# Set working directory
WORKDIR /var/www

EXPOSE 9000

USER $user

CMD ["php-fpm"]
